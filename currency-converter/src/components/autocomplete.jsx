
import React, { useEffect ,useRef} from 'react';
import '../style/autocomplete.css'

const SuggestionsList = props => {  
  const wrapperRef = useRef(null);
  useOutsideAlerter(wrapperRef);
    const {
      suggestions,
      inputValue,
      onSelectSuggestion,
      displaySuggestions,
      selectedSuggestion,
      onChangeCurrencyAutocomplete,
      label,
      onClickCurrencyAutocomplete,
      handleOnBlur
    } = props;
// console.log('props',props);

function useOutsideAlerter(ref) {
  function handleClickOutside(event) {
        
    if (ref.current && !ref.current.contains(event.target)) {
        //alert("You clicked outside of me!");
        //console.log('ref.current',ref.current);
        //console.log('ref.event.target',event.target);
        handleOnBlur()
      
    }else{
     // handleOnBlur()
    }
}
  useEffect(() => {
      /**
       * Alert if clicked on outside of element
       */

      // Bind the event listener
      document.addEventListener("mousedown", handleClickOutside);
      return () => {
          // Unbind the event listener on clean up
          document.removeEventListener("mousedown", handleClickOutside);
      };
  }, [ref]);
}
const renderSuggestionList = ()=>{  
  if (displaySuggestions) {    
      if (suggestions.length > 0) {
        return (
          <ul className="suggestions-list">
            {suggestions.map((suggestion, index) => {
              const isSelected = selectedSuggestion === index;
              const classname = `suggestion ${isSelected ? "selected" : ""}`;
              return (
                <li
                  key={index}
                  className={classname}
                  onClick={() => onSelectSuggestion(index)}
                >
                  {suggestion}
                </li>
              );
            })}
          </ul>
        );
      } else {
        return <div>No suggestions available...</div>;
      }
    }
  
}
   
    
    return <div  ref={wrapperRef}>    <h1>{props.label}</h1>
    <input
   
      className="user-input"
      id={props.label}
      type="text"
      onChange={onChangeCurrencyAutocomplete}
      onClick={onClickCurrencyAutocomplete}
      value={inputValue}
     // onBlur={handleOnBlur}
    />
  {  renderSuggestionList()}
    </div>;
  };

  export default SuggestionsList;