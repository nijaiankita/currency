import React from 'react';

const InputText = (props) => {
    const inputProps = {...props};

    return (
        <div className={'currency-input-field'}>
            <label id={props.id+'Label'}>
                <h1>{props.label}</h1>
                <input {...inputProps} 
                className="user-input"/>   
                <i className="currency-txtbox-spinner"/>
            </label>
        </div>
    );
}
export default InputText;
