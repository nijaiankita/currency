import React, { Component, } from "react";
import InputText from './components/inputText';
import CurrencyAutocomplete from './components/autocomplete';
import './style/currencyconvertor.css';

const suggestions = [
    "AUD",
    "USD",
    "CAD",
    "CNY",
    "CZK",
    "DKK",
    "EUR",
    "GBP",
    "JPY",
    "NOK",
    "NZD",

  ];

  const currencyPairs = 
   { 'AUD-USD' :0.8371,
    'CAD-USD' : 0.8711,
    'USD-CNY' : 6.1715,
    'EUR-USD' : 1.2315,
    'GBP-USD' : 1.5683,
    'NZD-USD' : 0.7750,
    'USD-JPY' : 119.95,
    'EUR-CZK' : 27.6028,
    'EUR-DKK' : 7.4405,
    'EUR-NOK' : 8.6651
    }

  ;
 
export default class Currency extends Component{
    constructor(props){
        super(props);
        this.state = {
            localAmount : 1,
            inputValue:'AUD',
            displaySuggestions:false,
            filteredSuggestions:[],
            selectedSuggestion:0,
            inputValueTo:'USD',
            selectedSuggestionTo:0,
            displaySuggestionsTo:false,
            filteredSuggestionsTo:[],
            isClickedFromCurrency:false,
            isClickedToCurrency:false,
            localAmountError:false,
            fromCurrencyError:'',
            toCurrencyError:''

        }
    }


    componentDidMount(){
        this.renderAmountCalculation();
    }

    getConvertedAmout = (amount,fromCurrency,toCurrency)=>{

       let fromAndTo = fromCurrency+'-'+toCurrency;
       let fromAndToInv = toCurrency+'-'+fromCurrency;       
       if(currencyPairs.hasOwnProperty(fromAndTo)){
        return amount*currencyPairs[fromAndTo]
       }else if(currencyPairs.hasOwnProperty(fromAndToInv)){
        let result = (1/((amount)*(currencyPairs[fromAndToInv])))
        return result;
       }else if(fromCurrency === toCurrency){
        return amount;
       }else{
        let fromCurrencyMatchPair = '';
        let toCurrencyMatchPair = '';

        Object.keys(currencyPairs).map(data=>{
            if(data.includes(fromCurrency)){
                fromCurrencyMatchPair  = data
            }
            if(data.includes(toCurrency)){
                toCurrencyMatchPair = data
            }
        })
        if(toCurrencyMatchPair && toCurrencyMatchPair.split('-')[1] ===  toCurrency){   

            let afterSplit = toCurrencyMatchPair.split('-')[0];
            let firstSplitPair = fromCurrency+'-'+afterSplit;
            let secondSplitPair = afterSplit+'-'+fromCurrency;            
            if(currencyPairs.hasOwnProperty(firstSplitPair)){
                return (amount *(currencyPairs[firstSplitPair]) *(currencyPairs[toCurrencyMatchPair]))

            }else if(currencyPairs.hasOwnProperty(secondSplitPair)){

                let invOfToCurrency = (1/((currencyPairs[secondSplitPair])));
                let result = (((amount)* (invOfToCurrency) *(currencyPairs[toCurrencyMatchPair])))
                
                return result
               
            }
        }else{
           console.log(' in else part');
           let afterSplit = fromCurrencyMatchPair.split('-')[1];
            
            let secondSplitPair = toCurrency+'-'+afterSplit;
            let firstSplitPair = afterSplit+'-'+toCurrency;
            if(currencyPairs.hasOwnProperty(firstSplitPair)){
                console.log('hiiii',currencyPairs[firstSplitPair]);
                console.log('hii00000000ii',currencyPairs[fromCurrencyMatchPair]);
                
                return (amount *(currencyPairs[firstSplitPair]) *(currencyPairs[fromCurrencyMatchPair]))

            }else if(currencyPairs.hasOwnProperty(secondSplitPair)){
                
                let invOfToCurrency = (1/((currencyPairs[secondSplitPair])));
                console.log('byyyy',invOfToCurrency);
                console.log('currencyPairs[fromCurrencyMatchPair]',currencyPairs[fromCurrencyMatchPair]);
                
                let result = (((amount)* (invOfToCurrency) *(currencyPairs[fromCurrencyMatchPair])))
                
                return result
               
            }
           
            
            
        }


            
          }
    }
    renderAmountCalculation =  () =>{
        let amount = this.state.localAmount !== undefined && this.state.localAmount !== '' ? this.state.localAmount : 0.00;
        let fromCurrency = this.state.inputValue;
        let toCurrency = this.state.inputValueTo;        
        let decimalIs = toCurrency !== undefined && toCurrency !== '' && toCurrency === 'JPY' ? 0 : 2
        let convertedAmount =  this.getConvertedAmout(amount,fromCurrency,toCurrency)  ;
              
        //let convertedAmount = 0.8371
      
    return <div className='col-md-12'>
       
        {(this.state.fromCurrencyError === '') && (this.state.toCurrencyError === '') && convertedAmount !== undefined
    ?
        <div className='currency-result'>{`${amount} ${'  ' } ${fromCurrency} ${'  =' } ${Number(convertedAmount !== undefined ? convertedAmount : 0).toFixed(decimalIs)} ${'  ' } ${toCurrency}`}</div>
        :
        <div style={{color :'#FF0000'}}> Please Select Currency</div>
        }
      
       

    </div>
    }
    
    handleOnChange =(value)=>{
        var patt = new RegExp(/^[0-9]{0,5}(\.{0,1})[0-9]{0,30}$/);
        this.setState({
            localAmountError :false
        },()=>{
            if (patt.test(value)) {
                this.setState({localAmount:value},()=>{    
                    this.renderAmountCalculation();     
                })
            }else {
                value.replace(/^[0-9]{0,5}(\.{0,1})[0-9]{0,30}$/, '');
            }

            if(value === ''){                     
                this.setState({localAmountError : 'Please enter valid amount'});
            }
        })
       
        
       
        
    }
    onClickCurrencyAutocompleteTo = () =>{
        //console.log('callled on',suggestions);
        let value = this.state.inputValueTo;
        const filteredSuggestions = suggestions.filter(suggestion =>
            suggestion.toLowerCase().includes(value.toLowerCase())
          );
          this.setState({
            isClickedToCurrency:true,
            filteredSuggestionsTo:filteredSuggestions,
            displaySuggestionsTo:true
        })
    }

    onClickCurrencyAutocomplete = () =>{
       // console.log('callled on click',suggestions);
       
        let value = this.state.inputValue;
        const filteredSuggestions = suggestions.filter(suggestion =>
            suggestion.toLowerCase().includes(value.toLowerCase())
          );
          this.setState({
            isClickedFromCurrency:true,
            filteredSuggestions:filteredSuggestions,
            displaySuggestions:true
        })
    }
    handleOnBlur = () =>{
        if(!this.state.isClickedFromCurrency){

        }
        this.setState({
            displaySuggestions:false,
            filteredSuggestions:[],
        })
    }
    handleOnBlurTo = () =>{
        if(!this.state.isClickedToCurrency){
            
        }
        this.setState({
            displaySuggestionsTo:false,
            filteredSuggestionsTo:[],
        })
    }

    onChangeCurrencyAutocomplete = event => {

        const value = event.target.value.toUpperCase();

        this.setState({fromCurrencyError :'',inputValue:value})    
        const filteredSuggestions = suggestions.filter(suggestion =>
          suggestion.toLowerCase().includes(value.toLowerCase())
        );
    
        this.setState({
            filteredSuggestions:filteredSuggestions,
            displaySuggestions:true
        })
        if(value == ''){
            this.setState({ fromCurrencyError : 'From Currency Can Not Be Empty' })
           
        }
      };

      onChangeCurrencyAutocompleteTo = event => {
        const value = event.target.value.toUpperCase();
        this.setState({toCurrencyError :''  ,inputValueTo:value})    
        const filteredSuggestions = suggestions.filter(suggestion =>
          suggestion.toLowerCase().includes(value.toLowerCase())
        );
    
        this.setState({
            filteredSuggestionsTo:filteredSuggestions,
            displaySuggestionsTo:true
        });
        if(value == ''){
            this.setState({ toCurrencyError : 'ToCurrency Can Not Be Empty' })
           
        }
      };


       onSelectSuggestion = index => {
           this.setState({
            fromCurrencyError:'',
            selectedSuggestion:0,
            inputValue:this.state.filteredSuggestions[index],
            filteredSuggestions:[],
            displaySuggestions:false
           })
      };
      onSelectSuggestionTo = index => {
        this.setState({
        toCurrencyError:"",
         selectedSuggestionTo:0,
         inputValueTo:this.state.filteredSuggestionsTo[index],
         filteredSuggestionsTo:[],
         displaySuggestionsTo:false
        })
   };
   
       render(){
        return(<>
        <div className="row">
           <div className="col-md-4">
            <InputText
                type='text'
                id="userAmount"
                label="Amount"
                onChange={(e) => this.handleOnChange(e.target.value)} 
                value={this.state.localAmount}
                

            />
            <div className="erro" style={{color :'#FF0000'}}>
            {this.state.localAmountError &&
            this.state.localAmountError


            }
            </div>
            
            </div>

           
                <div className="col-md-4">
                <CurrencyAutocomplete
                label = {'From'}
                inputValue={this.state.inputValue}
                selectedSuggestion={this.state.selectedSuggestion}
                onSelectSuggestion={this.onSelectSuggestion}
                displaySuggestions={this.state.displaySuggestions}
                suggestions={this.state.filteredSuggestions}
                onChangeCurrencyAutocomplete= {this.onChangeCurrencyAutocomplete}
                onClickCurrencyAutocomplete={this.onClickCurrencyAutocomplete}
                handleOnBlur={this.handleOnBlur}
                isClicked={this.state.isClickedFromCurrency}
                
            />
            <div className="erro" style={{color :'#FF0000'}}>
            {this.state.fromCurrencyError &&
            this.state.fromCurrencyError


            }
            </div>
                </div>
           
                <div className="col-md-4">
                <CurrencyAutocomplete
                label = {'To'}
                inputValue={this.state.inputValueTo}
                selectedSuggestion={this.state.selectedSuggestionTo}
                onSelectSuggestion={this.onSelectSuggestionTo}
                displaySuggestions={this.state.displaySuggestionsTo}
                suggestions={this.state.filteredSuggestionsTo}
                onChangeCurrencyAutocomplete= {this.onChangeCurrencyAutocompleteTo}
                onClickCurrencyAutocomplete={this.onClickCurrencyAutocompleteTo}
                handleOnBlur={this.handleOnBlurTo}
                isClicked={this.state.isClickedToCurrency}
                
            />
               
               <div className="erro" style={{color :'#FF0000'}}>
            {this.state.toCurrencyError &&
            this.state.toCurrencyError


            }
            </div>
           </div>
            
           

           
            
        </div>
         <div className='row'>
         {   this.renderAmountCalculation()}
        {/* {   this.state.localAmount + ' ' +this.state.inputValue + ' = '+  + this.state.inputValueTo} */}
        </div>
        </>

        )
    }
}